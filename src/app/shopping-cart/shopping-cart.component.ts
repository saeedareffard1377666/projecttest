import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  // listCart:Item_SHOP[];
  // Item_SHOPs:{ProductName,ProductPrice,ProductCategoryID,ProductImageSRC}[];
  // Item_SHOPs2:{ProductName,ProductPrice,ProductCategoryID,ProductImageSRC}[];
  listCartArray=[];
  constructor(private cartService:CartService) { }
  displayedColumns: string[] = ['item', 'cost'];
  transactions: Transaction[] = [
    {item: 'Beach ball', cost: 4},
    {item: 'Towel', cost: 5},
    {item: 'Frisbee', cost: 2},
    {item: 'Sunscreen', cost: 4},
    {item: 'Cooler', cost: 25},
    {item: 'Swim suit', cost: 15},
  ];

  /** Gets the total cost of all transactions. */
  getTotalCost() {
    return this.transactions.map(t => t.cost).reduce((acc, value) => acc + value, 0);
  }
  ngOnInit(): void {
    this.cartService.CartListEmmiter.subscribe(didactivated =>{
      console.log(didactivated);
      // this.Item_SHOPs2=[ new Item_SHOP(didactivated.ProductName,didactivated.ProductPrice,didactivated.ProductCategoryID,didactivated.ProductImageSRC)]
      var object={
        ProductName:didactivated.ProductName,
        ProductPrice:didactivated.ProductPrice,
        ProductCategoryID:didactivated.ProductCategoryID,
        ProductImageSRC:didactivated.ProductImageSRC
      }
      if(this.listCartArray.find(( {ProductName})=> ProductName===object.ProductName)==undefined){
        this.listCartArray.push(object);
        var i=this.listCartArray.length-1;
        this.cartService.CartarrayEm.emit(this.listCartArray);
        // while(i>0){
        //   this.cartService.CartListEmmiter.emit(this.listCartArray[i]);
        // }
      }
     
      // this.listCart+=didactivated;
      // this.Item_SHOPs=[ new Item_SHOP(didactivated.ProductName,didactivated.ProductPrice,didactivated.ProductCategoryID,didactivated.ProductImageSRC)]
      // this.listCart.push(didactivated);
      console.log(this.listCartArray);
    })
  }
  DeleteItem(theItem){
    // console.log(theItem);
    //  console.log(this.transactions.findIndex(({item})=>item===theItem));
     this.transactions=this.transactions.splice((this.transactions.findIndex(({item})=>item===theItem)+1));
     console.log(this.transactions);
     

  }


}
export interface Transaction {
  item: string;
  cost: number;
}
class Item_SHOP{
  constructor(public  ProductName:string,public ProductPrice:string, public ProductCategoryID:number, public ProductImageSRC:string){
  }
 }