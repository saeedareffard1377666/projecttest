import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  CartListEmmiter=new EventEmitter<Item_SHOP>();
  CartarrayEm=new EventEmitter<Item_SHOP[]>();
  constructor() { }
}
class Item_SHOP{
  constructor(public  ProductName:string,public ProductPrice:string, public ProductCategoryID:number, public ProductImageSRC:string){
  }
 }
