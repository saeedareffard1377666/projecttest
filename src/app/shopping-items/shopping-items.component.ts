import { Component, OnInit ,Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CartService } from '../cart.service';
@Component({
  selector: 'app-shopping-items',
  templateUrl: './shopping-items.component.html',
  styleUrls: ['./shopping-items.component.css']
})
export class ShoppingItemsComponent implements OnInit,OnChanges {
  @Output() TheId=new EventEmitter<number>();
  @Input() CatId:number;
  id:number;
  Item_SHOPs:{ProductName,ProductPrice,ProductCategoryID,ProductImageSRC}[];
  constructor(private routh: ActivatedRoute,private r: Router,private cartService:CartService) { }
 
   searchs:{ProductName,ProductPrice,ProductCategoryID,ProductImageSRC}[];
  ngOnInit(): void {
    
    this.TheId = this.routh.snapshot.params.ID;
    this.Item_SHOPs=[new Item_SHOP('watch','$499',1,'https://images-na.ssl-images-amazon.com/images/G/01/appcore/watch/S6-SE-launch/AMZ_FamilyStripe_Series_6_GPS_Cellular._CB403905326_.png'),
    new Item_SHOP('Sony XBR-55A9G 55 Inch TV: MASTER Series BRAVIA OLED 4K Ultra HD Smart TV with HDR and Alexa Compatibility','$229',1,'https://m.media-amazon.com/images/I/91j8ZfpDC1L._AC_UL320_.jpg'),
    new Item_SHOP('Canon EOS Rebel T7 DSLR Camera with 18-55mm lens | Built-in Wi-Fi|24.1 MP CMOS Sensor | |DIGIC 4+ Image Processor and Full HD Videos','$449',1,'https://m.media-amazon.com/images/I/71EWRyqzw0L._AC_UY218_.jpg'),
    new Item_SHOP('Sony Noise Cancelling Headphones WHCH710N: Wireless Bluetooth Over the Ear Headset with Mic for Phone-Call, Black','$198',1,'https://m.media-amazon.com/images/I/51OF7sr7e6L._AC_UY218_.jpg'),

    new Item_SHOP('Champion Kids Clothes Sweatshirts Youth Heritage Fleece Pull On Hoody Sweatshirt with Hood','$21.4',2,'https://m.media-amazon.com/images/I/81J5VEBBHSL._AC_UL320_.jpg'),
    new Item_SHOP('Columbia Baby Boys Snowtop II Bunting','$29.99',2,'https://m.media-amazon.com/images/I/61zdRYtPBML._AC_UL320_.jpg'),
    new Item_SHOP("Columbia Women's Benton Springs Full Zip Fleece Jacket",'$43.95',2,'https://m.media-amazon.com/images/I/51rMMdO2OYL._AC_UL320_.jpg'),
    new Item_SHOP("Amazon Essentials Men's Regular-fit Cotton Pique Polo Shirt",'$12',2,'https://m.media-amazon.com/images/I/91PgdFDORRL._AC_UL320_.jpg')
  
  ]
  
  // this.TheId.subscribe(didactivate =>{
  //   this.id=didactivate;
  // });
  this.searchs=[
  new Item_SHOP('Sony XBR-55A9G 55 Inch TV: MASTER Series BRAVIA OLED 4K Ultra HD Smart TV with HDR and Alexa Compatibility','$229',2,'https://m.media-amazon.com/images/I/91j8ZfpDC1L._AC_UL320_.jpg')

   
]
  // console.log(this.Item_SHOPs);
  this.TheId = this.routh.snapshot.params.ID;
  console.log(this.CatId);
  this.searchs=this.Item_SHOPs.filter( ({ ProductCategoryID }) => ProductCategoryID === +this.TheId ) ;
  console.log(this.searchs);
  }
  ngOnChanges(changes: SimpleChanges) {
    // this.id=this.CatId;
    if (this.CatId) {
      this.emittheevent(this.CatId);
    }
    // throw new Error('Method not implemented.');
  }

  emittheevent(asd){
    this.id=this.CatId;
    console.log(this.CatId);
    this.searchs=this.Item_SHOPs.filter( ({ ProductCategoryID }) => ProductCategoryID === +this.CatId ) ;
    console.log(this.searchs);
    // this.TheId.emit(this.CatId);

  }
  AddtoCart(cart){
    console.log(cart._elementRef.nativeElement.id);
    this.Item_SHOPs.find( ({ ProductName }) => ProductName === cart._elementRef.nativeElement.id ) ;
    this.cartService.CartListEmmiter.emit(this.Item_SHOPs.find( ({ ProductName }) => ProductName === cart._elementRef.nativeElement.id ));

  }

}
  class Item_SHOP{
   constructor(public  ProductName:string,public ProductPrice:string, public ProductCategoryID:number, public ProductImageSRC:string){
   }
  }

  